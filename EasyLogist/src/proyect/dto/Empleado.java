package proyect.dto;
import java.io.Serializable;

public class Empleado implements Serializable {	
	private int cod_empleado;
	private String cargo_empleado;
	private int cod_jefe;
	private String nombre_empleado;
	private String apellido_empleado;
	private String fecha_nac_empleado;
	private String usuario;
	private String contraseña;
	private int cod_tienda_area;
	public int getCod_empleado() {
		return cod_empleado;
	}
	public void setCod_empleado(int cod_empleado) {
		this.cod_empleado = cod_empleado;
	}
	public String getCargo_empleado() {
		return cargo_empleado;
	}
	public void setCargo_empleado(String cargo_empleado) {
		this.cargo_empleado = cargo_empleado;
	}
	public int getCod_jefe() {
		return cod_jefe;
	}
	public void setCod_jefe(int cod_jefe) {
		this.cod_jefe = cod_jefe;
	}
	public String getNombre_empleado() {
		return nombre_empleado;
	}
	public void setNombre_empleado(String nombre_empleado) {
		this.nombre_empleado = nombre_empleado;
	}
	public String getApellido_empleado() {
		return apellido_empleado;
	}
	public void setApellido_empleado(String apellido_empleado) {
		this.apellido_empleado = apellido_empleado;
	}
	public String getFecha_nac_empleado() {
		return fecha_nac_empleado;
	}
	public void setFecha_nac_empleado(String fecha_nac_empleado) {
		this.fecha_nac_empleado = fecha_nac_empleado;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContraseña() {
		return contraseña;
	}
	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}
	public int getCod_tienda_area() {
		return cod_tienda_area;
	}
	public void setCod_tienda_area(int cod_tienda_area) {
		this.cod_tienda_area = cod_tienda_area;
	}
	
	
	
}
	