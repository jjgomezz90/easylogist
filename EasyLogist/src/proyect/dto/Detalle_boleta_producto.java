package proyect.dto;
import java.io.Serializable;

public class Detalle_boleta_producto implements Serializable {	
	private String nombre_producto;
	private double precio_unitario;
	private double total_producto;
	
	private int cod_boleta;
	private int cod_producto;
	private int cantidad_producto;
	private int cantidad_por_producto;
	private int Precio_producto;
	
	
	
	public int getPrecio_producto() {
		return Precio_producto;
	}
	public void setPrecio_producto(int precio_producto) {
		Precio_producto = precio_producto;
	}
	public String getNombre_producto() {
		return nombre_producto;
	}
	public void setNombre_producto(String nombre_producto) {
		this.nombre_producto = nombre_producto;
	}
	public double getPrecio_unitario() {
		return precio_unitario;
	}
	public void setPrecio_unitario(double precio_unitario) {
		this.precio_unitario = precio_unitario;
	}
	public double getTotal_producto() {
		return total_producto;
	}
	public void setTotal_producto(double total_producto) {
		this.total_producto = total_producto;
	}
	public int getCantidad_por_producto() {
		return cantidad_por_producto;
	}
	public void setCantidad_por_producto(int cantidad_por_producto) {
		this.cantidad_por_producto = cantidad_por_producto;
	}
	public int getCod_boleta() {
		return cod_boleta;
	}
	public void setCod_boleta(int cod_boleta) {
		this.cod_boleta = cod_boleta;
	}
	public int getCod_producto() {
		return cod_producto;
	}
	public void setCod_producto(int cod_producto) {
		this.cod_producto = cod_producto;
	}
	public int getCantidad_producto() {
		return cantidad_producto;
	}
	public void setCantidad_producto(int cantidad_producto) {
		this.cantidad_producto = cantidad_producto;
	}
}
	