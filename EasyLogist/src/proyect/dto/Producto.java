package proyect.dto;
import java.io.Serializable;

public class Producto implements Serializable {	
	private int cod_producto;
	private String nombre_producto;
	private double precio_producto;
	private String marca_producto;
	private int cod_tienda;
	private int cantidad_producto;
	public int getCod_producto() {
		return cod_producto;
	}
	public void setCod_producto(int cod_producto) {
		this.cod_producto = cod_producto;
	}
	public String getNombre_producto() {
		return nombre_producto;
	}
	public void setNombre_producto(String nombre_producto) {
		this.nombre_producto = nombre_producto;
	}
	public double getPrecio_producto() {
		return precio_producto;
	}
	public void setPrecio_producto(double precio_producto) {
		this.precio_producto = precio_producto;
	}
	public String getMarca_producto() {
		return marca_producto;
	}
	public void setMarca_producto(String marca_producto) {
		this.marca_producto = marca_producto;
	}
	public int getCod_tienda() {
		return cod_tienda;
	}
	public void setCod_tienda(int cod_tienda) {
		this.cod_tienda = cod_tienda;
	}
	public int getCantidad_producto() {
		return cantidad_producto;
	}
	public void setCantidad_producto(int cantidad_producto) {
		this.cantidad_producto = cantidad_producto;
	}
}
	