package proyect.dto;
import java.io.Serializable;

public class Tienda implements Serializable {	
	private int cod_tienda;
	private String nombre_tienda;
	private int cod_distrito;
	public int getCod_tienda() {
		return cod_tienda;
	}
	public void setCod_tienda(int cod_tienda) {
		this.cod_tienda = cod_tienda;
	}
	public String getNombre_tienda() {
		return nombre_tienda;
	}
	public void setNombre_tienda(String nombre_tienda) {
		this.nombre_tienda = nombre_tienda;
	}
	public int getCod_distrito() {
		return cod_distrito;
	}
	public void setCod_distrito(int cod_distrito) {
		this.cod_distrito = cod_distrito;
	}
	
	
}
	