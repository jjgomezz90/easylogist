package proyect.dto;
import java.io.Serializable;

public class Boleta implements Serializable {	
	private int cod_boleta;
	private int cod_empleado;
	private String fecha_boleta;
	private double monto_total_boleta;
	private String Nombre_empleado;
	
	
	public String getNombre_empleado() {
		return Nombre_empleado;
	}
	public void setNombre_empleado(String nombre_empleado) {
		Nombre_empleado = nombre_empleado;
	}
	public int getCod_boleta() {
		return cod_boleta;
	}
	public void setCod_boleta(int cod_boleta) {
		this.cod_boleta = cod_boleta;
	}
	public int getCod_empleado() {
		return cod_empleado;
	}
	public void setCod_empleado(int cod_empleado) {
		this.cod_empleado = cod_empleado;
	}
	public String getFecha_boleta() {
		return fecha_boleta;
	}
	public void setFecha_boleta(String fecha_boleta) {
		this.fecha_boleta = fecha_boleta;
	}
	public double getMonto_total_boleta() {
		return monto_total_boleta;
	}
	public void setMonto_total_boleta(double monto_total_boleta) {
		this.monto_total_boleta = monto_total_boleta;
	}
}
	