package proyect.dao;

import proyect.dao.ConexionMYSQL;
import proyect.dto.Boleta;
import proyect.dto.Detalle_boleta_producto;
import proyect.dto.Empleado;
import proyect.dto.Producto;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
public class DAO implements IF {
	public ConexionMYSQL conexion = new ConexionMYSQL();


	public Boleta getUltimaBoleta() {
		PreparedStatement pstmt=null;
		Connection con = null;
		ResultSet rs=null;
		Boleta a = null;
		String sql = "select max(cod_boleta)" +
				"from boleta";

		con = conexion.getConnection();
		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			a= new Boleta();
			while (rs.next() ) {
				a.setCod_boleta(rs.getInt(1));
			}
		}
				 catch (SQLException e) {
						e.printStackTrace();
					} finally {
						try {
							rs.close();
							pstmt.close();
							con.close();
						} catch (SQLException e) {
							e.printStackTrace();

						}
					}
		return a; 
	}


	public boolean esUsuarioValido(String usuario, String password) {
		PreparedStatement pstmt = null;
		Connection con = null;
		ResultSet rs = null; 
		String sql = "select * from empleado where usuario=? and contrase�a=?";
		
		boolean  esValido = false;
		
		con = conexion.getConnection();
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, usuario);
			pstmt.setString(2, password);
			rs = pstmt.executeQuery();

			if ( rs.next() ) {
				esValido = true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
			rs.close();
			pstmt.close();
			con.close();
			} catch ( Exception e) {} 
		}

		return esValido;
	}
	
	public List getDetalle_boleta_producto(int cod_boleta) {
		PreparedStatement pstmt=null;
		Connection con = null;
		ResultSet rs=null;
		//Detalle_boleta_producto a = null;
		List lc = new ArrayList();
		String sql = "select detalle_boleta_producto.cod_producto, producto.nombre_producto, " +
				"detalle_boleta_producto.cantidad_producto_comprado, producto.precio_producto,  " +
				"(producto.precio_producto*detalle_boleta_producto.cantidad_producto_comprado) " +
				"from producto inner join detalle_boleta_producto " +
				"on producto.cod_producto=detalle_boleta_producto.cod_producto " +
				"where detalle_boleta_producto.cod_boleta=?";
		// {"C�digo", "Nombre" ,"Cantidad","Precio unitario","Total"};
		con = conexion.getConnection();
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, cod_boleta);
			rs = pstmt.executeQuery();
			//a= new Detalle_boleta_producto();
			while (rs.next() ) {
				Detalle_boleta_producto a = new Detalle_boleta_producto(); 
				a.setCod_producto(rs.getInt(1));
				a.setNombre_producto(rs.getString(2));
				a.setCantidad_producto(rs.getInt(3));
				a.setPrecio_unitario(rs.getDouble(4));
				a.setTotal_producto(rs.getDouble(5));
				lc.add(a);	
				
				int cod_producto=a.getCod_producto();
			}
		}
				 catch (SQLException e) {
						e.printStackTrace();
					} finally {
						try {
							rs.close();
							pstmt.close();
							con.close();
						} catch (SQLException e) {
							e.printStackTrace();

						}
					}
		return lc;
	}



	public void RegistrarBoleta(Boleta boleta) {
		PreparedStatement pstmt = null;
		Connection con = conexion.getConnection();	
		String sql1="insert into boleta " +
				"values(?,now(),?,?)";

		try {
			
		pstmt = con.prepareStatement(sql1);
		pstmt.setInt(1, boleta.getCod_boleta());
		pstmt.setDouble(2, boleta.getMonto_total_boleta());
		pstmt.setInt(3, boleta.getCod_empleado());
		pstmt.executeUpdate();
	}
	catch (SQLException e) {
		System.out.println("Ocurrio un error al insertar en Boleta--> " + e.getLocalizedMessage() );
		e.printStackTrace();
	} finally {
		try {
			pstmt.close();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	}


	@Override
	public void RegistrarDetalle_boleta(Detalle_boleta_producto detalle_boleta_producto) {
		PreparedStatement pstmt = null;
		Connection con = conexion.getConnection();	
		String sql1="insert into detalle_boleta_producto " +
				"values(?,?,?)";

		try {
			
		pstmt = con.prepareStatement(sql1);
		pstmt.setInt(1, detalle_boleta_producto.getCod_boleta());
		pstmt.setDouble(2, detalle_boleta_producto.getCod_producto());
		pstmt.setInt(3, detalle_boleta_producto.getCantidad_producto());
		pstmt.executeUpdate();
	}
	catch (SQLException e) {
		System.out.println("Ocurrio un error al insertar en Detalle_boleta--> " + e.getLocalizedMessage() );
		e.printStackTrace();
	} finally {
		try {
			pstmt.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	}


	@Override
	public Producto getProducto(int cod_producto) {
		PreparedStatement pstmt=null;
		Connection con = null;
		ResultSet rs=null;
		Producto a = null;
		String sql = "select * from producto " +
				"where cod_producto=?";

		con = conexion.getConnection();
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, cod_producto);
			rs = pstmt.executeQuery();
			a= new Producto();
			while (rs.next() ) {
				a.setCod_producto(rs.getInt(1));
				a.setNombre_producto(rs.getString(2));
				a.setPrecio_producto( rs.getInt(3));
				a.setMarca_producto( rs.getString(4));
				a.setCantidad_producto(rs.getInt(5));
				a.setCod_tienda(rs.getInt(6));
				
			}
		}
				 catch (SQLException e) {
						e.printStackTrace();
					} finally {
						try {
							rs.close();
							pstmt.close();
							con.close();
						} catch (SQLException e) {
							e.printStackTrace();

						}
					}
		return a; //retornar elproducto
	}


	@Override
	public Empleado getEmpleado(String usuario, String clave) {
		PreparedStatement pstmt=null;
		Connection con = null;
		ResultSet rs=null;
		Empleado a = null;
		String sql = "select  cod_empleado, nombre_empleado, cargo_empleado from empleado " +
				"where usuario=? and contrase�a=?";

		con = conexion.getConnection();
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, usuario);
			pstmt.setString(2, clave);
			
			rs = pstmt.executeQuery();
			a= new Empleado();
			while (rs.next() ) {
				a.setCod_empleado(rs.getInt(1));
				a.setNombre_empleado(rs.getString(2));
				a.setCargo_empleado(rs.getString(3));
			}
		}
				 catch (SQLException e) {
						e.printStackTrace();
					} finally {
						try {
							rs.close();
							pstmt.close();
							con.close();
						} catch (SQLException e) {
							e.printStackTrace();

						}
					}
		return a; 
	}

	public void commit() {
		PreparedStatement pstmt = null;
		Connection con = null;
		ResultSet rs = null; 
		String sql = "commit";
		try{
			con = conexion.getConnection();	
			pstmt = con.prepareStatement(sql);
			pstmt.executeUpdate();	
}  catch (SQLException e) {
	e.printStackTrace();
} finally {
	try {
		pstmt.close();
		con.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}	
	}	
	}
	
	@Override
	public void CancelarBoleta(int cod_boleta) {
		PreparedStatement pstmt = null;
		Connection con = null;
		ResultSet rs = null; 
		String sql = "delete from detalle_boleta_producto " +
				"where cod_boleta=?";
		try{
			con = conexion.getConnection();	
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, cod_boleta);
			pstmt.executeUpdate();	
}  catch (SQLException e) {
	e.printStackTrace();
} finally {
	try {
		pstmt.close();
		con.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}	
	}	
	}


	@Override
	public Boleta getMontoTotal(int cod_boleta) {
		PreparedStatement pstmt=null;
		Connection con = null;
		ResultSet rs=null;
		Boleta a = new Boleta();
		String sql = "select sum(producto.precio_producto*detalle_boleta_producto.cantidad_producto_comprado) " +
				"from producto inner join detalle_boleta_producto " +
				"on producto.cod_producto=detalle_boleta_producto.cod_producto " +
				"where detalle_boleta_producto.cod_boleta=?";

		con = conexion.getConnection();
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, cod_boleta);
			rs = pstmt.executeQuery();
			while (rs.next() ) {
				a.setMonto_total_boleta(rs.getDouble(1));
			}
		}
				 catch (SQLException e) {
						e.printStackTrace();
					} finally {
						try {
							rs.close();
							pstmt.close();
							con.close();
						} catch (SQLException e) {
							e.printStackTrace();

						}
					}
		return a; 
	}


	@Override
	public Detalle_boleta_producto cantidad_comprada(int cod_producto) {
		PreparedStatement pstmt=null;
		Connection con = null;
		ResultSet rs=null;
		Detalle_boleta_producto a = null;
		String sql = "select cantidad_producto_comprado " +
				"from detalle_boleta_producto " +
				"where cod_producto=?";

		con = conexion.getConnection();
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1,cod_producto);
			rs = pstmt.executeQuery();
			a= new Detalle_boleta_producto();
			while (rs.next() ) {
				a.setCantidad_producto(rs.getInt(1));  //cantidad de producto comprado
			}
		}
				 catch (SQLException e) {
						e.printStackTrace();
					} finally {
						try {
							rs.close();
							pstmt.close();
							con.close();
						} catch (SQLException e) {
							e.printStackTrace();

						}
					}
		return a; 
	}


	@Override
	public Producto getCantidadproducto(int cod_producto) {
		PreparedStatement pstmt=null;
		Connection con = null;
		ResultSet rs=null;
		Producto a = null;
		String sql = "select cantidad_producto " +
				"from producto " +
				"where cod_producto=?";

		con = conexion.getConnection();
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1,cod_producto);
			rs = pstmt.executeQuery();
			a= new Producto();
			while (rs.next() ) {
				a.setCantidad_producto(rs.getInt(1));  //cantidad de producto comprado
			}
		}
				 catch (SQLException e) {
						e.printStackTrace();
					} finally {
						try {
							rs.close();
							pstmt.close();
							con.close();
						} catch (SQLException e) {
							e.printStackTrace();

						}
					}
		return a; 
	}
	
	public void updateCantidadProducto(int cod_producto, int cantidad_producto) {
		PreparedStatement pstmt = null;
		Connection con = null;
		String sql = "update producto " +
				"set cantidad_producto=? " +
				"where cod_producto=?";
		try{
			con = conexion.getConnection();	
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, cantidad_producto);
			pstmt.setInt(2, cod_producto);
			pstmt.executeUpdate();	
}  catch (SQLException e) {
	e.printStackTrace();
} finally {
	try {
		pstmt.close();
		con.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}	
	}	
	}


	@Override
	public List<Detalle_boleta_producto> getCod_productos_actualizar(int cod_boleta) {
		PreparedStatement pstmt=null;
		Connection con = null;
		ResultSet rs=null;
		
		List <Detalle_boleta_producto> lc = new ArrayList();
		String sql = "select cod_producto from detalle_boleta_producto " +
				"where cod_boleta=?";
		con = conexion.getConnection();
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, cod_boleta);
			rs = pstmt.executeQuery();
			while (rs.next() ) {
				Detalle_boleta_producto a = new Detalle_boleta_producto();
				a.setCod_producto(rs.getInt(1));
				lc.add(a);	
			}
		}
				 catch (SQLException e) {
						e.printStackTrace();
					} finally {
						try {
							rs.close();
							pstmt.close();
							con.close();
						} catch (SQLException e) {
							e.printStackTrace();

						}
					}
		return lc;
	}


	@Override
	public List getAllBoletas() {
		PreparedStatement pstmt=null;
		Connection con = null;
		ResultSet rs=null;
		List lc = new ArrayList();
		String sql = "select boleta.cod_boleta,boleta.fecha_boleta,boleta.monto_total_boleta, empleado.nombre_empleado " +
				"from empleado inner join boleta " +
				"on empleado.cod_empleado= boleta.cod_empleado";
		// {"C�digo", "Nombre" ,"Cantidad","Precio unitario","Total"};
		con = conexion.getConnection();
		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next() ) {
				Boleta a = new Boleta(); 
				a.setCod_boleta(rs.getInt(1));
				a.setFecha_boleta(rs.getString(2));
				a.setMonto_total_boleta(rs.getDouble(3));
				a.setNombre_empleado(rs.getString(4));
				lc.add(a);	

			}
		}
				 catch (SQLException e) {
						e.printStackTrace();
					} finally {
						try {
							rs.close();
							pstmt.close();
							con.close();
						} catch (SQLException e) {
							e.printStackTrace();

						}
					}
		return lc;
	}


	public List getAllProductos() {
		PreparedStatement pstmt=null;
		Connection con = null;
		ResultSet rs=null;
		List lc = new ArrayList();
		String sql = "select cod_producto, nombre_producto, precio_producto, marca_producto, cantidad_producto " +
					 "from producto";
		// {"C�digo", "Nombre" ,"Cantidad","Precio unitario","Total"};
		con = conexion.getConnection();
		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next() ) {
				Producto a = new Producto(); 
				a.setCod_producto(rs.getInt(1));
				a.setNombre_producto(rs.getString(2));
				a.setPrecio_producto(rs.getDouble(3));
				a.setMarca_producto(rs.getString(4));
				lc.add(a);	

			}
		}
				 catch (SQLException e) {
						e.printStackTrace();
					} finally {
						try {
							rs.close();
							pstmt.close();
							con.close();
						} catch (SQLException e) {
							e.printStackTrace();

						}
					}
		return lc;
	}



	@Override
	public void SumarProductoCancelado(int cantidad_comprada, int cod_producto) {
		PreparedStatement pstmt = null;
		Connection con = null;
		String sql = "update producto " +
				"set cantidad_producto=cantidad_producto + ? " +
				"where cod_producto=?";
		try{
			con = conexion.getConnection();	
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, cantidad_comprada);
			pstmt.setInt(2, cod_producto);
			pstmt.executeUpdate();	
}  catch (SQLException e) {
	e.printStackTrace();
} finally {
	try {
		pstmt.close();
		con.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}	
	}
		
	}

	public Producto getUltimoProducto() {
		PreparedStatement pstmt=null;
		Connection con = null;
		ResultSet rs=null;
		Producto a = null;
		String sql = "select max(cod_producto) +1 " + 
					  "from producto";

		con = conexion.getConnection();
		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			a= new Producto();
			while (rs.next() ) {
				a.setCod_producto(rs.getInt(1));
			}
		}
				 catch (SQLException e) {
						e.printStackTrace();
					} finally {
						try {
							rs.close();
							pstmt.close();
							con.close();
						} catch (SQLException e) {
							e.printStackTrace();

						}
					}
		return a; 
	}


	@Override
	public void RegistrarProducto(Producto prod) {
		PreparedStatement pstmt = null;
		Connection con = conexion.getConnection();	
		String sql1="insert into producto " +
				"values(?,?,?,?,?,?)";

		try {
			
		pstmt = con.prepareStatement(sql1);
		pstmt.setInt(1, prod.getCod_producto());
		pstmt.setString(2, prod.getNombre_producto());
		pstmt.setDouble(3, prod.getPrecio_producto());
		pstmt.setString(4, prod.getMarca_producto());
		pstmt.setInt(5, prod.getCantidad_producto());
		pstmt.setInt(6, prod.getCod_tienda());
		
		pstmt.executeUpdate();
	}
	catch (SQLException e) {
		System.out.println("Ocurrio un error al insertar en Boleta--> " + e.getLocalizedMessage() );
		e.printStackTrace();
	} finally {
		try {
			pstmt.close();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
		
	}
}
