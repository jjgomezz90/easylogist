package proyect.dao;

import java.sql.Connection;
import java.sql.DriverManager;

// Esta clase recupera la conexi�n
public final class ConexionMYSQL {

	//private final String url = "jdbc:mysql://localhost:3306/securitysystem?user=root&password=root";
	private final String url = "jdbc:mysql://localhost:3306/easylogist?user=root&password=root";

	public Connection getConnection() {
		Connection con = null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			
			con = DriverManager.getConnection(url);
			
		} catch ( Exception e) {
			System.out.println("* * * Ocurrio un ERROR " + e.getLocalizedMessage() );
			e.printStackTrace();
		}
		
		return con;
		
	}
}
