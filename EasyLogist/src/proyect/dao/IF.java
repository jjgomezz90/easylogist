package proyect.dao;
import java.util.List;

import proyect.dto.Boleta;
import proyect.dto.Detalle_boleta_producto;
import proyect.dto.Empleado;
import proyect.dto.Producto;

//getDetalle_boleta_producto
public interface IF {
	
	public Boleta getUltimaBoleta(); 
	public List getDetalle_boleta_producto(int cod_boleta);
	public void RegistrarBoleta(Boleta boleta);
	public void RegistrarDetalle_boleta(Detalle_boleta_producto detalle_boleta_producto);
	public Producto getProducto(int cod_producto);
	public boolean esUsuarioValido(String usuario, String password);
	public Empleado getEmpleado(String usuario, String clave); 
	public void CancelarBoleta(int cod_boleta);
	public void commit();
	public Boleta getMontoTotal(int cod_boleta);
	public Detalle_boleta_producto cantidad_comprada(int cod_producto); //hallar la cantidad de producto que va a comprar
	public Producto getCantidadproducto(int cod_producto);// hallar la cantidad de producto que hay actualmente en stock
	public void updateCantidadProducto(int cod_producto, int cantidad_producto);  //cambiar la cantidad de 
	public void SumarProductoCancelado(int cantidad_comprada, int cod_producto);
	public List <Detalle_boleta_producto> getCod_productos_actualizar(int cod_boleta);									//producto despues de la compra
	public List getAllBoletas();
	public List getAllProductos();
	public Producto getUltimoProducto();
	public void RegistrarProducto(Producto prod);


	
	
}
