package proyect.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import proyect.dao.DAO;
import proyect.dao.IF;
import proyect.dto.Boleta;
import proyect.dto.Detalle_boleta_producto;
import proyect.dto.Producto;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.SystemColor;
import java.awt.Color;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;


public class RegistrarVentaMain extends JFrame implements ActionListener {

	private int cod_boleta_actual=0;
	int numero_articulos_comprados=0;
	int[] cant_productos= new int[100]; //es un valor al azar, cantidad de maxima de articulos por boleta
	int[] arreglo_cod_producto = new int[100];

	public int getCod_boleta_actual() {
		return cod_boleta_actual;
	}

	private JPanel jContentPane     = null;
	private JTable tblFlights       = new JTable();
	private JScrollPane jScrollPane = new JScrollPane();
	private JLabel etiqueta = new JLabel();
	private RegistrarVentaModel flightModel = new RegistrarVentaModel();
	private final JLabel label = new JLabel("C\u00F3digo de boleta");
	private final JLabel label_1 = new JLabel("Fecha:");
	private final JLabel label_2 = new JLabel("Nombre del vendedor:");
	private final JTextField nombre_vendedor_text = new JTextField();
	private final JLabel label_3 = new JLabel("Easy Logist");
	private final JLabel lblCdigoDeProducto = new JLabel("C\u00F3digo de producto:");
	private final JTextField cod_producto_buscar = new JTextField();
	private final JTextField codigo_boleto_text = new JTextField();
	private final JTextField fecha_text = new JTextField();
	private final JPanel panel = new JPanel();
	private final JLabel lblNewLabel = new JLabel("Producto");
	private final JTextField text_nombre_producto = new JTextField();
	private final JLabel lblPrecio = new JLabel("Precio");
	private final JTextField text_precio_producto = new JTextField();
	private final JLabel lblStock = new JLabel("Stock:");
	private final JTextField text_stock_restante = new JTextField();
	private final JLabel lblCantidadAComprar = new JLabel("Cantidad a comprar:");
	private final JTextField text_cantidad_comprar = new JTextField();
	private JTextField text_total_boleta;
	private final JMenuBar menuBar = new JMenuBar();
	private final JMenu mnInicio = new JMenu("Inicio");
	private final JMenu mnVentas = new JMenu("Ventas");
	private final JMenu mnInventario = new JMenu("Inventario");
	private final JMenuItem mntmCerrarSesin = new JMenuItem("Cerrar sesi\u00F3n");
	private final JMenuItem mntmInformeDeBoletas = new JMenuItem("Informe de boletas");
	private final JMenu mnIngreso = new JMenu("Ingreso");
	private final JMenuItem mntmMensual = new JMenuItem("Mensual");
	private final JMenuItem mntmSemanal = new JMenuItem("Semanal");
	private final JMenuItem mntmDiario = new JMenuItem("Diario");
	private final JMenuItem mntmAnual = new JMenuItem("Anual");
	private final JMenuItem mntmEstadisticas = new JMenuItem("Estadisticas");
	private final JMenuItem mntmRegistroDeInventario = new JMenuItem("Registrar nuevo producto");
	private final JMenuItem mntmListaDeInventario = new JMenuItem("Lista de inventario");
	private final JMenu mnRegistroDeProductos = new JMenu("Registro de productos");
	private final JMenuItem mntmRegistrarIngresoDe = new JMenuItem("Registrar ingreso de producto");
	
	// Mostrar el Logo
	
	public static void main(String[] args) {
		RegistrarVentaMain f = new RegistrarVentaMain();
		f.setVisible(true);
	}

	public RegistrarVentaMain() {
		super();
		text_cantidad_comprar.setColumns(10);
		text_stock_restante.setEditable(false);
		text_stock_restante.setColumns(10);
		text_precio_producto.setEditable(false);
		text_precio_producto.setColumns(10);
		text_nombre_producto.setEditable(false);
		text_nombre_producto.setColumns(10);
		fecha_text.setEditable(false);
		fecha_text.setColumns(10);
		codigo_boleto_text.setEditable(false);
		codigo_boleto_text.setColumns(10);
		cod_producto_buscar.setColumns(10);
		label_3.setFont(new Font("Maiandra GD", Font.BOLD, 20));
		nombre_vendedor_text.setText(IniciarSesion.nombre_empleado);
		nombre_vendedor_text.setEditable(false);
		nombre_vendedor_text.setColumns(10);
		initialize();
	}

	private void initialize() {
		this.setSize(818, 582);
		
		setJMenuBar(menuBar);
		
		menuBar.add(mnInicio);
		mntmCerrarSesin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int m = JOptionPane
						.showConfirmDialog(
								null,
								"�Esta seguro que desea salir de la aplicaci�n?",
								"ALERTA", JOptionPane.YES_NO_OPTION);
				if (m == 0) {
				setVisible(false);
	            dispose();
				IniciarSesion jframe_sub = new IniciarSesion(); 
	            jframe_sub.setVisible(true); 
				}
			}
		});
		
		mnInicio.add(mntmCerrarSesin);
		
		menuBar.add(mnVentas);
		mntmInformeDeBoletas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {	
				System.out.println();
					MostrarBoletasMain jframe_sub = new MostrarBoletasMain(); 
		            jframe_sub.setVisible(true); 

	            
	            
	            
	            
			}
		});
		
		mnVentas.add(mntmInformeDeBoletas);
		mnIngreso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		mnVentas.add(mnIngreso);
		
		mnIngreso.add(mntmAnual);
		
		mnIngreso.add(mntmMensual);
		
		mnIngreso.add(mntmSemanal);
		
		mnIngreso.add(mntmDiario);
		
		menuBar.add(mnInventario);
		
		mnInventario.add(mnRegistroDeProductos);
		mnRegistroDeProductos.add(mntmRegistroDeInventario);
		mntmRegistrarIngresoDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RegistroIngresoMain jframe_sub = new RegistroIngresoMain(); 
	            jframe_sub.setVisible(true); 				
				
				
			}
		});
		
		mnRegistroDeProductos.add(mntmRegistrarIngresoDe);
		mntmRegistroDeInventario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				System.out.println(IniciarSesion.cargo_empleado);
					RegistroInventarioMain jframe_sub = new RegistroInventarioMain(); 
		            jframe_sub.setVisible(true); 	
				
			}
		});
		
		mnInventario.add(mntmEstadisticas);
		mntmListaDeInventario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MostrarInventarioMain jframe_sub = new MostrarInventarioMain(); 
	            jframe_sub.setVisible(true); 				
				
				
			}
		});
		
		mnInventario.add(mntmListaDeInventario);
		this.setContentPane(getJContentPane());
		this.setTitle("Registrar Venta");
		IF dao = new DAO();
		Boleta bol = dao.getUltimaBoleta();
		int boleta_cod=bol.getCod_boleta();
		cod_boleta_actual=boleta_cod+1;
		String codigo_boleta=Integer.toString(cod_boleta_actual);
		codigo_boleto_text.setText(codigo_boleta);

		//AQUI LE DAS EL VALOR A LA BOLETA
		IF dao1 = new DAO();  //para inicializar valores
		Calendar cal1 = Calendar.getInstance();
		fecha_text.setText(""+cal1.get(Calendar.DATE)+"/"+cal1.get(Calendar.MONTH)
			    +"/"+cal1.get(Calendar.YEAR));
		// La tabla de vuelos
		//tblFlights.getSelectionModel().addListSelectionListener(this);
		tblFlights.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		// Configuramos el uso de Un AbstractTableModel para la tabla
		tblFlights.setModel(flightModel);

		// El sroll panel
		jScrollPane.setViewportView(tblFlights);

	}
	

	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setBackground(new Color(112, 128, 144));
			
			JButton btnBuscar = new JButton("Buscar");
			btnBuscar.setFont(new Font("Tahoma", Font.BOLD, 11));
			btnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					int cod_producto=0;
					try{
						cod_producto=Integer.parseInt(cod_producto_buscar.getText());
					}catch (NumberFormatException e){
						
					}
					IF dao = new DAO();
					Producto producto= new Producto();
					producto=dao.getProducto(cod_producto);
					text_nombre_producto.setText(producto.getNombre_producto());
					
					//String precio_producto=Integer.toString((int) producto.getPrecio_producto()); //COMO CHUCHA LO PONGO DOUBLE :(
					String precio_producto= producto.getPrecio_producto()+ "";
					
					
					text_precio_producto.setText(precio_producto);
					 
					 
					String producto_restante=Integer.toString(producto.getCantidad_producto());
					text_stock_restante.setText(producto_restante);		
				}
			});
			
			JButton btnAgregar = new JButton("Agregar");
			btnAgregar.setFont(new Font("Tahoma", Font.BOLD, 11));
			btnAgregar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String nombre_usuario=IniciarSesion.usuario;
						int cod_empleado=IniciarSesion.getCodigo_empleado(); //hallar el codigo del empleado
						int cod_producto=Integer.parseInt(cod_producto_buscar.getText());  // hallar codigo de producto a insertar
						int cant_comprar=0;
						cant_comprar=Integer.parseInt(text_cantidad_comprar.getText());  //registrar cantidad a comprar
						numero_articulos_comprados=numero_articulos_comprados+1;//para almacenar en el arreglo a la hora de cancelar la boleta
						IF dao = new DAO();
						Producto producto= new Producto();
						producto=dao.getProducto(cod_producto);
						double Precio_producto=producto.getPrecio_producto();
						
						Detalle_boleta_producto detalle=new Detalle_boleta_producto();
						
						//para actualizar los registros a la hora de buscar
						
						Producto prod1 = dao.getCantidadproducto(cod_producto);
						int cantidad_stock =prod1.getCantidad_producto(); //para hallar la cantidad real en stock
						int cantidad_nueva_stock=cantidad_stock-cant_comprar;
						cant_productos[numero_articulos_comprados] = cant_comprar;/*ESTO ES EL ARREGLO, SE VA ACTUALIZANDO 
																			LA CANTIDAD QUE SE COMPRO DE CADA ARTICULO PARA QUE SE ACTUALIZE
																		 CUANDO SE BORRA LA BOLETA */
						arreglo_cod_producto[numero_articulos_comprados]=cod_producto;
						/*
						System.out.println("cantidad_stock :" + cantidad_stock);
						System.out.println("cantidad_comprar: " + cant_comprar);
						System.out.println("cantidad_nueva_stock :" + cantidad_nueva_stock);
						*/
						if (cantidad_nueva_stock>0) {
							//System.out.println("Correcto");
							//dar valores al bean
							detalle.setCod_boleta(cod_boleta_actual);
							detalle.setCod_producto(cod_producto);
							detalle.setCantidad_producto(cant_comprar);
							dao.RegistrarDetalle_boleta(detalle);
							
							text_nombre_producto.setText("");
							text_precio_producto.setText("");
							text_stock_restante.setText("");
							cod_producto_buscar.setText("");
							text_cantidad_comprar.setText("");
							
							dao.updateCantidadProducto(cod_producto, cantidad_nueva_stock); // actualizar nueva cantidad en stock

					List l = dao.getDetalle_boleta_producto(cod_boleta_actual);
					// Pasamos la data al DataModel
					flightModel.updateData(l);
					
					Boleta bol1 = dao.getMontoTotal(cod_boleta_actual);
					Double monto_boleta=bol1.getMonto_total_boleta();
					String monto_boleta_string = Integer.toString((int) bol1.getMonto_total_boleta());
					text_total_boleta.setText(monto_boleta_string);//para hallar el monto total

						} else {
							//System.out.println("Incorrecto");
				JOptionPane.showMessageDialog(null,"Cantidad demandada es mayor al stock del producto","ERROR",JOptionPane.ERROR_MESSAGE);
						}
						
					}
					
				
			});
			
			JButton btnConfirmar = new JButton("Confirmar");
			btnConfirmar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {	
				IF dao = new DAO();
				int i=0;
				List <Detalle_boleta_producto> lc=dao.getCod_productos_actualizar(cod_boleta_actual);
				
				Boleta boleta = dao.getMontoTotal(cod_boleta_actual);  // para hallar el monto total de la boleta
				double monto_boleta= boleta.getMonto_total_boleta(); 
				boleta.setCod_boleta(cod_boleta_actual);
				boleta.setCod_empleado(IniciarSesion.codigo_empleado);//hacer un inicio de sesion
				boleta.setMonto_total_boleta(monto_boleta);
				dao.RegistrarBoleta(boleta);  //registrar la boleta
				numero_articulos_comprados=0;//para reactualizar el arreglo
				JOptionPane.showMessageDialog(null,"Transacci�n registrada","Confirmado",JOptionPane.INFORMATION_MESSAGE);
				actualizarInterfaz();

				}
			});
			
			JButton btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					int m = JOptionPane
							.showConfirmDialog(
									null,
									"�Esta seguro que desa cancelar la transacci�n?",
									"ALERTA", JOptionPane.YES_NO_OPTION);
					if (m == 0) {
						IF dao = new DAO();

						int k=1;
						while(k<=numero_articulos_comprados) {
							dao.SumarProductoCancelado(cant_productos[k], arreglo_cod_producto[k]);
							k=k+1;
						}
						
						dao.CancelarBoleta(cod_boleta_actual);	
						cod_producto_buscar.setText("");
						text_cantidad_comprar.setText("");
						text_nombre_producto.setText("");
						text_precio_producto.setText("");
						text_stock_restante.setText("");
						text_total_boleta.setText("");
						// Pasamos la data al DataModel
						List l = dao.getDetalle_boleta_producto(cod_boleta_actual);
						flightModel.updateData(l);
						dao.commit();
						JOptionPane.showMessageDialog(null,"Transaccion cancelada","Confirmado",JOptionPane.INFORMATION_MESSAGE);	
					}
					
					}
			});
			
			JLabel Total = new JLabel("Total (S/.) : ");
			Total.setFont(new Font("Tahoma", Font.BOLD, 11));
			
			text_total_boleta = new JTextField();
			text_total_boleta.setEditable(false);
			text_total_boleta.setColumns(10);
			GroupLayout gl_jContentPane = new GroupLayout(jContentPane);
			gl_jContentPane.setHorizontalGroup(
				gl_jContentPane.createParallelGroup(Alignment.TRAILING)
					.addComponent(etiqueta)
					.addGroup(gl_jContentPane.createSequentialGroup()
						.addContainerGap()
						.addComponent(panel, GroupLayout.DEFAULT_SIZE, 907, Short.MAX_VALUE)
						.addGap(30))
					.addGroup(gl_jContentPane.createSequentialGroup()
						.addContainerGap()
						.addComponent(jScrollPane, GroupLayout.DEFAULT_SIZE, 907, Short.MAX_VALUE)
						.addGap(30))
					.addGroup(gl_jContentPane.createSequentialGroup()
						.addContainerGap(711, Short.MAX_VALUE)
						.addComponent(btnConfirmar)
						.addGap(30)
						.addComponent(btnCancelar)
						.addGap(52))
					.addGroup(gl_jContentPane.createSequentialGroup()
						.addContainerGap(644, Short.MAX_VALUE)
						.addComponent(Total, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)
						.addGap(18)
						.addComponent(text_total_boleta, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
						.addGap(38))
					.addGroup(gl_jContentPane.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_jContentPane.createParallelGroup(Alignment.LEADING)
							.addComponent(label_3, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_jContentPane.createSequentialGroup()
								.addGroup(gl_jContentPane.createParallelGroup(Alignment.LEADING)
									.addComponent(label_2, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
									.addComponent(lblCdigoDeProducto)
									.addComponent(lblCantidadAComprar))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_jContentPane.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_jContentPane.createSequentialGroup()
										.addGroup(gl_jContentPane.createParallelGroup(Alignment.LEADING)
											.addComponent(cod_producto_buscar, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE)
											.addComponent(text_cantidad_comprar, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE))
										.addGap(51)
										.addGroup(gl_jContentPane.createParallelGroup(Alignment.LEADING)
											.addComponent(btnAgregar)
											.addComponent(btnBuscar)))
									.addComponent(nombre_vendedor_text, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE))))
						.addGap(231)
						.addGroup(gl_jContentPane.createParallelGroup(Alignment.TRAILING, false)
							.addGroup(gl_jContentPane.createSequentialGroup()
								.addComponent(label, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED))
							.addGroup(gl_jContentPane.createSequentialGroup()
								.addComponent(label_1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addGap(89)))
						.addGroup(gl_jContentPane.createParallelGroup(Alignment.LEADING)
							.addComponent(codigo_boleto_text, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_jContentPane.createSequentialGroup()
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(fecha_text, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)))
						.addGap(61))
			);
			gl_jContentPane.setVerticalGroup(
				gl_jContentPane.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_jContentPane.createSequentialGroup()
						.addGroup(gl_jContentPane.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_jContentPane.createSequentialGroup()
								.addComponent(etiqueta)
								.addGap(21)
								.addGroup(gl_jContentPane.createParallelGroup(Alignment.BASELINE)
									.addComponent(codigo_boleto_text, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(label))
								.addGap(18)
								.addGroup(gl_jContentPane.createParallelGroup(Alignment.BASELINE)
									.addComponent(fecha_text, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(label_1)
									.addComponent(label_2)
									.addComponent(nombre_vendedor_text, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
							.addGroup(gl_jContentPane.createSequentialGroup()
								.addContainerGap()
								.addComponent(label_3, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
						.addGap(18)
						.addGroup(gl_jContentPane.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblCdigoDeProducto)
							.addComponent(btnBuscar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(cod_producto_buscar, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_jContentPane.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblCantidadAComprar)
							.addComponent(btnAgregar)
							.addComponent(text_cantidad_comprar, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addGap(18)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
						.addGap(28)
						.addComponent(jScrollPane, GroupLayout.PREFERRED_SIZE, 206, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_jContentPane.createParallelGroup(Alignment.BASELINE)
							.addComponent(text_total_boleta, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(Total))
						.addGap(15)
						.addGroup(gl_jContentPane.createParallelGroup(Alignment.BASELINE)
							.addComponent(btnCancelar)
							.addComponent(btnConfirmar))
						.addGap(25))
			);
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addContainerGap()
						.addComponent(lblNewLabel)
						.addGap(18)
						.addComponent(text_nombre_producto, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(lblPrecio)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(text_precio_producto, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(lblStock)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(text_stock_restante, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(212, Short.MAX_VALUE))
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblNewLabel)
							.addComponent(text_nombre_producto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(lblPrecio)
							.addComponent(text_precio_producto, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
							.addComponent(lblStock)
							.addComponent(text_stock_restante, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(15, Short.MAX_VALUE))
			);
			panel.setLayout(gl_panel);
			jContentPane.setLayout(gl_jContentPane);
		}
		return jContentPane;
	}
public void actualizarInterfaz() {
	IF dao = new DAO();
	Boleta bol = dao.getUltimaBoleta();
	int boleta_cod=bol.getCod_boleta();
	cod_boleta_actual=boleta_cod+1;
	String codigo_boleta=Integer.toString(cod_boleta_actual);
	codigo_boleto_text.setText(codigo_boleta);
	List l = dao.getDetalle_boleta_producto(cod_boleta_actual);
	cod_producto_buscar.setText("");
	text_cantidad_comprar.setText("");
	text_nombre_producto.setText("");
	text_precio_producto.setText("");
	text_stock_restante.setText("");
	text_total_boleta.setText("");//para hallar el monto total

	// Pasamos la data al DataModel
	flightModel.updateData(l);

}
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
}
