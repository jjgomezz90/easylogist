package proyect.view;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

import proyect.dto.Boleta;

/*
 * La clase AbstractTableModel implementa la interface TableModel
 */
public class MostrarBoletasModel extends AbstractTableModel {
	// Las cabeceras de las columnas
	private final String[] columnNames = {"C�digo de boleta", "Fecha/hora","Monto total","Empleado responsable"};
	
	// la lista de datos
	private List data = new ArrayList();
	
	public void updateData(List l)	{
		data = l;
		fireTableDataChanged();  //***************NO SE USA
	}	
	
	public List getData() {
		return data;	
	}
	
	/*
	 * Los siguientes metodos hacen que las CELDAS de la tabla
	 * NO sean EDITABLES
	 * Si se desea modificar valores hay que sobreescribir los metodos
	 *  setValueAt y isCellEditable
	 */
	public int getRowCount() {
		return data.size();
	}

	public int getColumnCount()	{
		return columnNames.length;
	}
	
	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col)	{
		switch(col) { 
		case 0: return ((Boleta) data.get(row)).getCod_boleta();
		case 1:  return ((Boleta) data.get(row)).getFecha_boleta();
		case 2: return ((Boleta) data.get(row)).getMonto_total_boleta();  // corregir validar en BOLETA
		case 3: return ((Boleta) data.get(row)).getNombre_empleado();    // Validar codigo de empleado (crear bean)
		}   
		return null;
	}
	
}
