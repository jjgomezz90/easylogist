package proyect.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JButton;

import proyect.dao.DAO;
import proyect.dao.IF;
import proyect.dto.Producto;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RegistroIngresoMain extends JFrame {

	private JPanel contentPane;
	private JTextField txt_cantidad_comprar;
	private JTextField txt_precio_producto;
	private JTextField txt_marca_producto;
	private JTextField txt_nombre_producto;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegistroIngresoMain frame = new RegistroIngresoMain();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegistroIngresoMain() {
		setTitle("Registrar nuevo producto");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 288, 203);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblNombreDeProducto = new JLabel("Nombre de producto:");
		
		JLabel lblMarcaDeProducto = new JLabel("Marca de producto:");
		
		JLabel lblPrecioDeProducto = new JLabel("Precio de producto:");
		
		JLabel lblCantidadAComprar = new JLabel("Cantidad a comprar:");
		
		txt_nombre_producto = new JTextField();
		txt_nombre_producto.setColumns(10);
		
		txt_marca_producto = new JTextField();
		txt_marca_producto.setColumns(10);
		
		txt_precio_producto = new JTextField();
		txt_precio_producto.setColumns(10);
		
		txt_cantidad_comprar = new JTextField();
		txt_cantidad_comprar.setColumns(10);
		
		JButton btnConfirmar = new JButton("Confirmar");
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IF dao = new DAO();
				Producto a=dao.getUltimoProducto();
				
				try{
					String nombre_producto=txt_nombre_producto.getText();
					int precio_producto=Integer.parseInt(txt_precio_producto.getText());
					String marca_producto=txt_marca_producto.getText();
					int cantidad_producto=Integer.parseInt(txt_cantidad_comprar.getText());
					int cod_producto=a.getCod_producto();
					
					Producto producto = new Producto();
					producto.setCod_producto(cod_producto);
					producto.setCantidad_producto(cantidad_producto);
					producto.setMarca_producto(marca_producto);
					producto.setPrecio_producto(precio_producto);
					producto.setNombre_producto(nombre_producto);
					
					dao.RegistrarProducto(producto);//aqui registra el producto
	
					
					setVisible(false);
		            dispose();
					MostrarInventarioMain jframe_sub = new MostrarInventarioMain();
		            jframe_sub.setVisible(true); 
					
				}catch (Exception e1){
				JOptionPane.showMessageDialog(null,"Error al ingresar el producto","ERROR",JOptionPane.ERROR_MESSAGE);	
					
				}

				
			}
		});
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txt_marca_producto.setText("");
				txt_cantidad_comprar.setText("");
				txt_precio_producto.setText("");
				txt_nombre_producto.setText("");
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNombreDeProducto)
								.addComponent(lblMarcaDeProducto)
								.addComponent(lblPrecioDeProducto)
								.addComponent(lblCantidadAComprar))
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(txt_precio_producto, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)
								.addComponent(txt_marca_producto, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)
								.addComponent(txt_nombre_producto, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)
								.addComponent(txt_cantidad_comprar, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)))
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnConfirmar)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnBorrar)))
					.addContainerGap(48, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNombreDeProducto)
						.addComponent(txt_nombre_producto, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblMarcaDeProducto)
						.addComponent(txt_marca_producto, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPrecioDeProducto)
						.addComponent(txt_precio_producto, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCantidadAComprar)
						.addComponent(txt_cantidad_comprar, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnBorrar)
						.addComponent(btnConfirmar))
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}
}
