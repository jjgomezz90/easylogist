package proyect.view;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;


import proyect.dto.Detalle_boleta_producto;
import proyect.dto.Empleado;


/*
 * La clase AbstractTableModel implementa la interface TableModel
 */
public class DetalleBoletaModel extends AbstractTableModel {
	// Las cabeceras de las columnas
	private final String[] columnNames = {"C�digo de articulo", "Nombre de art�culo", "Costo de art�culo"};
	
	// la lista de datos
	private List data = new ArrayList();
	
	public void updateData(List l)	{
		data = l;
		fireTableDataChanged();  //***************NO SE USA
	}	
	
	public List getData() {
		return data;	
	}
	
	/*
	 * Los siguientes metodos hacen que las CELDAS de la tabla
	 * NO sean EDITABLES
	 * Si se desea modificar valores hay que sobreescribir los metodos
	 *  setValueAt y isCellEditable
	 */
	public int getRowCount() {
		return data.size();
	}

	public int getColumnCount()	{
		return columnNames.length;
	}
	
	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col)	{
		switch(col) { 
		case 0: return ((Detalle_boleta_producto) data.get(row)).getCod_producto();
		case 1:  return ((Detalle_boleta_producto) data.get(row)).getNombre_producto();
		case 2: return ((Detalle_boleta_producto) data.get(row)).getCantidad_producto();
		case 3: return ((Detalle_boleta_producto) data.get(row)).getPrecio_producto();
		case 4: return ((Detalle_boleta_producto) data.get(row)).getTotal_producto();
		
		}
		return null;
	}
	
}
