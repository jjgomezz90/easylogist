package proyect.view;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

import proyect.dto.Producto;

/*
 * La clase AbstractTableModel implementa la interface TableModel
 */
public class MostrarInventarioModel extends AbstractTableModel {
	// Las cabeceras de las columnas
	private final String[] columnNames = {"C�digo de producto", "Nombre","Precio","Marca","Stock"};
	
	// la lista de datos
	private List data = new ArrayList();
	
	public void updateData(List l)	{
		data = l;
		fireTableDataChanged();  //***************NO SE USA
	}	
	
	public List getData() {
		return data;	
	}
	
	public int getRowCount() {
		return data.size();
	}

	public int getColumnCount()	{
		return columnNames.length;
	}
	
	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col)	{
		switch(col) { 
		case 0: return ((Producto) data.get(row)).getCod_producto();
		case 1: return ((Producto) data.get(row)).getNombre_producto();
		case 2: return ((Producto) data.get(row)).getPrecio_producto();
		case 3: return ((Producto) data.get(row)).getMarca_producto();
		case 4: return ((Producto) data.get(row)).getCantidad_producto();
		}   
		return null;
	}
	
}
