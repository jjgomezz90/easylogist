package proyect.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import proyect.dao.DAO;
import proyect.dao.IF;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextArea;
import java.awt.SystemColor;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Toolkit;


public class MostrarBoletasMain extends JFrame implements ActionListener {

	private JPanel jContentPane     = null;
	private JButton btnRefresh      = new JButton();
	private JTable tblFlights       = new JTable();
	private JScrollPane jScrollPane = new JScrollPane();
	private JLabel etiqueta = new JLabel();
	private MostrarBoletasModel flightModel = new MostrarBoletasModel();
	private JTextField cod_robo_text;
	
	public static int codigo_boleta;
	
	public static int getCodigo_boleta() {
		return codigo_boleta;
	}

	public static void setCodigo_robo(int codigo_boleta) {
		MostrarBoletasMain.codigo_boleta= codigo_boleta;
	}

	public static void main(String[] args) {
		MostrarBoletasMain f = new MostrarBoletasMain();
		f.setVisible(true);
	}

	public MostrarBoletasMain() {
		super();
		setIconImage(Toolkit.getDefaultToolkit().getImage(MostrarBoletasMain.class.getResource("/proyect/resources/lock-icon.png")));
		initialize();
	}

	private void initialize() {
		this.setSize(945, 409);
		this.setContentPane(getJContentPane());
		this.setTitle("Registro de boletas");
		btnRefresh.setFont(new Font("Microsoft YaHei", Font.BOLD, 11));

		// El boton de Refresh
		btnRefresh.setText("Actualizar");
		btnRefresh.setMnemonic(KeyEvent.VK_S);
		btnRefresh.addActionListener(this);

		// La tabla de vuelos
		//tblFlights.getSelectionModel().addListSelectionListener(this);
		tblFlights.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		// Configuramos el uso de Un AbstractTableModel para la tabla
		tblFlights.setModel(flightModel);

		// El sroll panel
		jScrollPane.setViewportView(tblFlights);

	}
	

	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			
			cod_robo_text = new JTextField();
			cod_robo_text.setColumns(10);
			
			JLabel lblCodigoDeRobo = new JLabel("C�digo de boleta:");
			lblCodigoDeRobo.setFont(new Font("Microsoft New Tai Lue", Font.PLAIN, 12));
			
			JTextArea txtrPara = new JTextArea();
			txtrPara.setBackground(SystemColor.control);
			txtrPara.setText("Para ver mas detalles de la boleta \r\n" +
					"inserte el c�digo de la boleta aqui:");
			
			JButton btnVerDetalles = new JButton("Ver detalles");
			btnVerDetalles.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					codigo_boleta=Integer.parseInt(cod_robo_text.getText());
					DetalleBoletaMain jframe_sub = new DetalleBoletaMain(); 
		            jframe_sub.setVisible(true);  
				}
			});
			btnVerDetalles.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			GroupLayout gl_jContentPane = new GroupLayout(jContentPane);
			gl_jContentPane.setHorizontalGroup(
				gl_jContentPane.createParallelGroup(Alignment.TRAILING)
					.addGroup(gl_jContentPane.createSequentialGroup()
						.addGroup(gl_jContentPane.createParallelGroup(Alignment.LEADING)
							.addComponent(etiqueta)
							.addGroup(gl_jContentPane.createSequentialGroup()
								.addContainerGap()
								.addGroup(gl_jContentPane.createParallelGroup(Alignment.LEADING)
									.addComponent(txtrPara, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE)
									.addComponent(jScrollPane, GroupLayout.PREFERRED_SIZE, 911, GroupLayout.PREFERRED_SIZE)
									.addGroup(gl_jContentPane.createSequentialGroup()
										.addGap(10)
										.addComponent(lblCodigoDeRobo, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(cod_robo_text, GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE)
										.addGap(18)
										.addComponent(btnVerDetalles)))))
						.addContainerGap(25, Short.MAX_VALUE))
					.addGroup(gl_jContentPane.createSequentialGroup()
						.addContainerGap(815, Short.MAX_VALUE)
						.addComponent(btnRefresh)
						.addGap(72))
			);
			gl_jContentPane.setVerticalGroup(
				gl_jContentPane.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_jContentPane.createSequentialGroup()
						.addGroup(gl_jContentPane.createParallelGroup(Alignment.LEADING)
							.addComponent(etiqueta)
							.addComponent(jScrollPane, GroupLayout.PREFERRED_SIZE, 219, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_jContentPane.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_jContentPane.createSequentialGroup()
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(btnRefresh, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_jContentPane.createSequentialGroup()
								.addGap(39)
								.addComponent(txtrPara, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_jContentPane.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblCodigoDeRobo, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
							.addComponent(cod_robo_text, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
							.addComponent(btnVerDetalles))
						.addGap(111))
			);
			jContentPane.setLayout(gl_jContentPane);
		}
		return jContentPane;
	}

	/*
	* Se ejecuta toda la acci�n aqui	
	*/
	public void actionPerformed(ActionEvent e)	{
		if (e.getSource() == btnRefresh ) {
			IF dao = new DAO();
			List l = dao.getAllBoletas();  //IMPLEMENTAR DAO
			// Pasamos la data al DataModel
			flightModel.updateData(l);
		} 
	}
}
