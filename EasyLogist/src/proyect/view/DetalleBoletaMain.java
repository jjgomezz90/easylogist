package proyect.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import proyect.dao.DAO;
import proyect.dao.IF;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Font;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.Toolkit;


public class DetalleBoletaMain extends JFrame implements ActionListener {

	private JPanel jContentPane     = null;
	private JButton btnRefresh      = new JButton();
	private JTable tblFlights       = new JTable();
	private JScrollPane jScrollPane = new JScrollPane();
	private JLabel etiqueta = new JLabel();
	private DetalleBoletaModel flightModel = new  DetalleBoletaModel();
	
	// Mostrar el Logo
	
	public static void main(String[] args) {
		DetalleBoletaMain f = new DetalleBoletaMain();
		f.setVisible(true);
	}

	public DetalleBoletaMain() {
		super();
		setIconImage(Toolkit.getDefaultToolkit().getImage(DetalleBoletaMain.class.getResource("/proyect/resources/lock-icon.png")));
		initialize();
	}

	private void initialize() {
		this.setSize(449, 162);
		this.setContentPane(getJContentPane());
		this.setTitle("Detalle de boleta");

		// El boton de Refresh
		btnRefresh.setText("Ver detalles");
		btnRefresh.setMnemonic(KeyEvent.VK_S);
		btnRefresh.addActionListener(this);

		// La tabla de vuelos
		//tblFlights.getSelectionModel().addListSelectionListener(this);
		tblFlights.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		// Configuramos el uso de Un AbstractTableModel para la tabla
		tblFlights.setModel(flightModel);

		// El sroll panel
		jScrollPane.setViewportView(tblFlights);

	}
	

	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			GroupLayout gl_jContentPane = new GroupLayout(jContentPane);
			gl_jContentPane.setHorizontalGroup(
				gl_jContentPane.createParallelGroup(Alignment.TRAILING)
					.addGroup(gl_jContentPane.createSequentialGroup()
						.addGroup(gl_jContentPane.createParallelGroup(Alignment.LEADING)
							.addComponent(etiqueta)
							.addGroup(gl_jContentPane.createSequentialGroup()
								.addContainerGap()
								.addGroup(gl_jContentPane.createParallelGroup(Alignment.TRAILING)
									.addComponent(btnRefresh)
									.addComponent(jScrollPane, GroupLayout.PREFERRED_SIZE, 420, GroupLayout.PREFERRED_SIZE))))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
			);
			gl_jContentPane.setVerticalGroup(
				gl_jContentPane.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_jContentPane.createSequentialGroup()
						.addGroup(gl_jContentPane.createParallelGroup(Alignment.LEADING)
							.addComponent(etiqueta)
							.addComponent(jScrollPane, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btnRefresh, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
						.addGap(278))
			);
			jContentPane.setLayout(gl_jContentPane);
		}
		return jContentPane;
	}

	/*
	* Se ejecuta toda la acci�n aqui	
	*/
	public void actionPerformed(ActionEvent e)	{
		if (e.getSource() == btnRefresh ) {
			IF dao = new DAO();
			List l = dao.getDetalle_boleta_producto(MostrarBoletasMain.codigo_boleta);
			flightModel.updateData(l);
		} 
	}
}
