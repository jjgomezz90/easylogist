package proyect.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JPasswordField;
import javax.swing.JButton;

import proyect.dao.DAO;
import proyect.dao.IF;
import proyect.dto.Empleado;

import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.awt.Color;
import javax.swing.UIManager;

public class IniciarSesion extends JFrame {

	private JPanel contentPane;
	private JTextField usuario_text;
	private JPasswordField password_text;

	private int contador=0;
	
	
	public static String sesion_iniciada="false";
	public static String usuario="";
	public static String clave="";
	public static String nombre_empleado="";
	public static String cargo_empleado="";
	public static int codigo_empleado=0;
	
	public static String getCargo_empleado() {
		return nombre_empleado;
	}

	public static void setCargo_empleado(String nombre_empleado) {
		IniciarSesion.nombre_empleado = nombre_empleado;
	}

	
	public static String getNombre_empleado() {
		return nombre_empleado;
	}

	public static void setNombre_empleado(String nombre_empleado) {
		IniciarSesion.nombre_empleado = nombre_empleado;
	}

	public static int getCodigo_empleado() {
		return codigo_empleado;
	}

	public static void setCodigo_empleado(int codigo_empleado) {
		IniciarSesion.codigo_empleado = codigo_empleado;
	}
	public static String getSesion_iniciada() {
		return sesion_iniciada;
	}

	public static void setSesion_iniciada(String sesion_iniciada) {
		IniciarSesion.sesion_iniciada = sesion_iniciada;
	}

	public static String getUsuario() {
		return usuario;
	}

	public static void setUsuario(String usuario) {
		IniciarSesion.usuario = usuario;
	}

	public static String getClave() {
		return clave;
	}

	public static void setClave(String clave) {
		IniciarSesion.clave = clave;
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IniciarSesion frame = new IniciarSesion();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public IniciarSesion() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(IniciarSesion.class.getResource("/proyect/resources/lock-icon.png")));
		setTitle("Iniciar Sesi\u00F3n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(112, 128, 144));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel label = new JLabel("Nombre de usuario:");
		
		usuario_text = new JTextField();
		usuario_text.setColumns(10);
		
		JLabel label_1 = new JLabel("Contrase\u00F1a");
		
		password_text = new JPasswordField();
		password_text.setColumns(10);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(169, 169, 169));
		
		JButton button = new JButton("Aceptar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				usuario=usuario_text.getText();
				clave=password_text.getText();
				IF dao = new DAO();
				
				if (dao.esUsuarioValido(usuario,clave)==true)
				{
					sesion_iniciada="true";
					Empleado emp = dao.getEmpleado(usuario,clave);
					codigo_empleado=emp.getCod_empleado();
					nombre_empleado=emp.getNombre_empleado();
					cargo_empleado=emp.getCargo_empleado();
					JOptionPane.showMessageDialog(null,"Usuario Valido","Bienvenido",JOptionPane.INFORMATION_MESSAGE);
					setVisible(false);
		            dispose();
					RegistrarVentaMain jframe_sub = new RegistrarVentaMain(); 
		            jframe_sub.setVisible(true);  
		          
				} else {
				if (contador<3) {
					JOptionPane.showMessageDialog(null,"Usuario o contrase�a inv�lido","ERROR",JOptionPane.ERROR_MESSAGE);
					contador=contador+1;
					sesion_iniciada="false";
				} else {
					JOptionPane.showMessageDialog(null,"Ha excedido el numero maximo de intentos para iniciar sesion","ERROR",JOptionPane.ERROR_MESSAGE);
					System.exit(0);
				}
				}
			}
		});
		
		JButton button_1 = new JButton("Limpiar");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				usuario_text.setText("");
				password_text.setText("");
			}
		});
		
		JButton button_2 = new JButton("Salir");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGap(0, 313, Short.MAX_VALUE)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(19)
					.addComponent(button)
					.addGap(31)
					.addComponent(button_1)
					.addGap(31)
					.addComponent(button_2, GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE)
					.addGap(21))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGap(0, 49, Short.MAX_VALUE)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(21)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(button_1)
						.addComponent(button)
						.addComponent(button_2))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(20)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(label, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE)
								.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(password_text, GroupLayout.PREFERRED_SIZE, 238, GroupLayout.PREFERRED_SIZE)
								.addComponent(usuario_text, GroupLayout.PREFERRED_SIZE, 238, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(40)
							.addComponent(panel, GroupLayout.PREFERRED_SIZE, 313, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(30, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(38)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(usuario_text, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(29)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_1)
						.addComponent(password_text, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 53, Short.MAX_VALUE)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addGap(43))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
